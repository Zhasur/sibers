import React, {Component} from 'react';
import {Button, ListGroup, ListGroupItem} from "reactstrap";

import './Contacts.css'
import Contact from "../../components/Contact/Contact";
import Spinner from "../../UI/Spinner/Spinner";
import Modal from "../../UI/Modal/Modal";
import GetContact from "../../components/GetContact/GetContact";
import Input from "reactstrap/es/Input";

class Contacts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            contacts: null,
            contact: null,
            isChosen: false,
            value:'',
            sort: false,
            search: '',
            isSearching: false
        };
    }

    // As I've read online docs componentWillMount comes after Constructor...
    // so here I am getting data, by one of ajax request (which is fetch), and setting them to my own state object
    componentWillMount() {
        if (!localStorage.getItem('contacts')) { //this condition states for: if there are not data in localStorage
                                                     // get data from the shown link, if there are some data in localStorage then use them...
            fetch("http://demo.sibers.com/users")
                .then(res => res.json())
                .then(
                    (result) => {
                        localStorage.setItem('contacts', JSON.stringify(result)); //However before getting data
                                                                                 // I am creating an array called contacts in localStorage
                                                                                // in order to make changes in future...
                        this.setState({
                            isLoaded: true,
                            contacts: JSON.parse(localStorage.getItem('contacts'))
                        }); // So I've got the data and pushed them all into my localStorage array
                    },
                    (error) => {
                        this.setState({
                            isLoaded: true,
                            error
                        });
                    }
                );
        } else {
            this.setState({
                isLoaded: true,
                contacts: JSON.parse(localStorage.getItem('contacts'))
            })
        }
    }

    onClickHandler = index => { // This function I am using to get the infos about the chosen contact
        const contact = this.state.contacts[index];
        this.showModal();
        this.setState({contact});
    };

    onUpdateHandler = (editContactData) => { // I want to explain it orally cuz there are bunch of things to write ;)
        let forEditContact = this.state.contacts.find((contact) => contact.id === editContactData.id);
        if (forEditContact) {
            forEditContact = editContactData;
        } else {
            throw new Error('Contact not found');
        }
        localStorage.setItem('contacts', JSON.stringify(this.state.contacts));
        this.setState({isChosen: false});
    };

    showModal = () => {
        this.setState({isChosen: true});
    };

    closeModal = () => {
        this.setState({isChosen: false});
    };

    updateContact = (contactEditData) => {
        console.log(contactEditData);
    };

    updateSearch = (event) => {
        this.setState({search: event.target.value.substr(0, 20)})
    };

    isSearchingToggle = () => {
        this.setState({isSearching: !this.state.isSearching});
    };

    render() {
        let contact;
        if (!this.state.isLoaded) {
            contact = <Spinner/>; // While there are no fetched data instead contacts appears Spinner :) also good trick btw...
        } else {
            contact = <Contact // in case if data fetched here I am calling Contact one by one
                search = {this.state.search}
                onclick={this.onClickHandler}
                contacts={this.state.contacts}
            />;
        }

        let searchInput;
        if (this.state.isSearching) {
            searchInput = <Input style={{marginTop: "6px"}} type="text" onChange={this.updateSearch}/>;
        } else {
            searchInput = null;
        }
        return (
            <div className="contacts">
                {this.state.isChosen &&
                <Modal
                    show={this.state.isChosen}
                    close={this.closeModal}
                >
                    <GetContact
                        contact={this.state.contact}
                        update={this.onUpdateHandler}
                        onUpdate={this.updateContact}
                    />
                </Modal>
                }
                <ListGroup>
                    <ListGroupItem style={{backgroundColor: "#17a2b8"}}>
                        <h4
                            style={{display: "inline-block", margin: 0, color: "#fff", lineHeight: "36px"}}
                        >
                            Contacts
                        </h4>
                        <Button color="info" outline
                                className="sort-btn"
                                onClick={this.isSearchingToggle}
                        ><i className="fas fa-search" style={{color: "#fff"}}/>
                        </Button>
                        {searchInput}
                    </ListGroupItem>
                    {contact}
                </ListGroup>
            </div>
        );
    }
}

export default Contacts;