import React from 'react';
import Contacts from "./containers/Contacts/Contacts";
import {Route, Switch} from "react-router-dom";
import './App.css';

class App extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Switch>
          <Route path="/" exact component={Contacts}/>
        </Switch>
      </React.Fragment>
    );
  }
}

export default App;
