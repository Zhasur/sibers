import React from 'react';
import {Button, Form} from "reactstrap";
import './GetContact.css'
import FormGroup from "reactstrap/es/FormGroup";
import Input from "reactstrap/es/Input";
import Label from "reactstrap/es/Label";

class GetContact extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            contact: this.props.contact
        };
    }

    componentWillMount() {
        this.setState({contact: this.props.contact});
    }

    // with the help of this changeHandler function I can change the value of chosen element
    changeHandler = (key) => ((event) => { // here I am using a key argument to specify which value of the element I want to change
        let newContact = this.state.contact;
        newContact[key] = event.target.value;
        this.setState({newContact});
    });

    onUpdateContact = () => {
        this.props.update(this.state.contact); //here I am calling an onUpdateHandler function from Contacts.js
    };

    render()
        {
            let contact;
            if (this.props.contact) {
                contact = (
                    <Form key={this.props.contact.id}>
                        <div style={{display: "block", margin: "0", textAlign: "center"}}>
                            <img style={{borderRadius: "50%"}} src={this.props.contact.avatar} alt="avatar"/>
                            <h4 style={{margin: "4px 0", padding: "10px", display: "block"}}>{this.state.contact.name}</h4>
                        </div>
                        <FormGroup className="get-info">
                            <Label className="get-info-inner get-info-inner-text">Name</Label>
                            <Input className="get-info-inner"
                                   value={this.props.contact.name}
                                   onChange={this.changeHandler("name")}
                            />
                        </FormGroup>
                        <FormGroup className="get-info">
                            <Label className="get-info-inner get-info-inner-text">Phone</Label>
                            <Input className="get-info-inner"
                                   value={this.props.contact.phone}
                                   onChange={this.changeHandler("phone")}
                            />
                        </FormGroup>
                        <FormGroup className="get-info">
                            <Label className="get-info-inner get-info-inner-text">Email</Label>
                            <Input className="get-info-inner" value={this.state.contact.email} onChange={this.changeHandler("email")}/>
                        </FormGroup>
                        <Button
                            onClick={this.onUpdateContact}
                            className="edit-btn"
                            outline color="secondary"
                        >Update
                        </Button>
                        <Button
                            className="edit-btn"
                            outline color="primary"
                        >Close
                        </Button>
                    </Form>
                )
            }
            return (
                [contact]
            );
        }
}

export default GetContact;