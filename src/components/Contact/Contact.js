import React, {Fragment} from 'react';
import {ListGroupItem} from "reactstrap";
import './Contact.css'



const Contact = (props) => {

    // Here I am getting contacts and by filter method I am getting filtered contacts by their naming

    let filteredContacts = props.contacts.filter(contact => {
        // here the indexOf prototype helps me to find whether there is a contact name which includes a value of props.search...
        return contact.name.toLowerCase().indexOf(props.search) !== -1;
    });

    //And here I am getting filtered contacts one by one and calling them inside of my ListGroupItem
    let contact = filteredContacts.map((contact, id) => (
            <ListGroupItem
                onClick={() => props.onclick(contact.id)}
                key={id}
                className="contact"
            >
                <div className="inner-block">
                    <img className="contact-avatar" src={contact.avatar} alt="avatar"/>
                </div>
                <div className="inner-block">
                    <h5 className="contact-name">{contact.name}</h5>
                    <span className="info"><i className="fas fa-phone"/>{contact.phone}</span>
                </div>
            </ListGroupItem>
        ));

    return (
        <Fragment>
            {contact}
        </Fragment>

    );
};

export default Contact;